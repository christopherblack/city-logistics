﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class appMenu : System.Web.UI.Page
{
    Cloud Cloud = new Cloud();
    LogiData LogiData = new LogiData();


    protected void Page_Load(object sender, EventArgs e)
    {
        Cloud.Page(this.Page);
        if (!IsPostBack)
        {
            initialize();
        }
    }

    public void initialize()
    {
    
    }

    public void btnFloorAudit_click(object sender, EventArgs e)
    {
        Response.Redirect("/appFloorAudit.aspx");
    }

}